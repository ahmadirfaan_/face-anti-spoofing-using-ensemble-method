# Face Anti-Spoofing using Ensemble Method

Using three models that can be considered as new state-of-the-art in Computer Vision for detecting Face Attacks in 2D images

You can use every dataset that has an PyTorch's ImageFolder structure, and don't forget to modify the code according to your preferred configurations and datasets.

The Idea from this method is taken from the paper: [Arxiv](https://arxiv.org/pdf/2112.11290.pdf)

You should read that paper and know why I taken this approach

## Credit

This repository was made to be my final task as an AI Engineer Intern at Nodeflux, kudos for the superb time there while being supervised by amazing mentors
